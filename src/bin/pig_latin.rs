//! Pig Latin translator
//!
//! Convert strings to Pig Latin, where the first consonant
//! of each word is moved to the end of the word with an
//! added “ay”, so “first” becomes “irst-fay”. Words that
//! start with a vowel get “hay” added to the end instead
//! (“apple” becomes “apple-hay”). Remember about UTF-8 encoding!

use std::io::{ self, Write };

fn main() {
    
    print!("Enter some words: ");
    io::stdout().flush().unwrap();

    let mut user_input = String::new();
    io::stdin()
        .read_line(&mut user_input)
        .expect("Failed to read form stdin");

    let mut words = user_input.split_whitespace();
    let mut pig_latin = String::new();
    for word in words {
        if !pig_latin.is_empty() {
            pig_latin.push(' ');
        }
        let pigified = pigify(word);
        pig_latin.push_str(&pigified);
    }
    println!("{}", pig_latin);
}

fn pigify(word: &str) -> String {
    let mut chars = word.chars();
    let first_char = chars.next().unwrap();
    match first_char {
        'a' | 'e' | 'i' | 'o' | 'u'  => format!("{}-hay", word),
        _ => format!("{}-{}ay", chars.as_str(), first_char),
    }
}
