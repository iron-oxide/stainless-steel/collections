//! stats
//!
//! Given a list of integers, use a vector and return the mean
//! (average), median (when sorted, the value in the middle position),
//! and mode (the value that occurs most often; a hash map will be
//! helpful here) of the list.

use std::io::{self, Write};
use std::collections::HashMap;

/// Read ints from user and calculate stats.
///
fn main() {

    print!("Enter some integers: ");
    io::stdout().flush().unwrap();

    let mut user_input = String::new();
    io::stdin()
        .read_line(&mut user_input)
        .expect("Failed to read from stdin");

    let mut integers: Vec<i64> = vec![];
    let integer_strings = user_input.split_whitespace();
    for integer in integer_strings {
        integers.push(integer.parse().unwrap());
    }

    let average: i64 = integers.iter().sum::<i64>() / integers.len() as i64;

    integers.sort();
    let median = integers[integers.len() / 2];
    let mode = {
        let mut counts = HashMap::new();
        for integer in integers.iter() {
            *counts.entry(integer).or_insert(0) += 1;
        }

        counts.into_iter()
            .max_by_key(|&(_, count)| count)
            .map(|(val, _)| val)
            .expect("Cannot compute mode of zero numbers")
    };

    println!("Average: {}", average);
    println!("Median: {}", median);
    println!("Mode: {}", mode);

}
