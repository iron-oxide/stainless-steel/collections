//! Employees
//!
//! Using a hash map and vectors, create a text interface to
//! allow a user to add employee names to a department in the
//! company. For example, “Add Sally to Engineering” or “Add
//! Amir to Sales”. Then let the user retrieve a list of all
//! people in a department or all people in the company by
//! department, sorted alphabetically.
//!
//! Note:
//!
//! This is a bad design, the only purpose is to practice
//! using `Vec` and `HashMap`, as well as Enums and Structs.

use std::io::{ self, Write };
use std::collections::HashMap;

fn main() {

    let mut company = Company {
        name: String::from("Starshell"),
        departments: HashMap::new(),
    };

    company.departments.insert(String::from("IT"), Vec::<Employee>::new());
    company.departments.insert(String::from("Accounting"), Vec::<Employee>::new());
    company.departments.insert(String::from("Software Engineering"), Vec::<Employee>::new());
    company.departments.insert(String::from("R&D"), Vec::<Employee>::new());
    company.departments.insert(String::from("Dev Ops"), Vec::<Employee>::new());
    company.departments.insert(String::from("Engineering"), Vec::<Employee>::new());
    company.departments.insert(String::from("Code Monkeys"), Vec::<Employee>::new());

    let exit = false;
    loop {
        let choice = menu_prompt();
        match choice {
            Choice::ADD_USER => add_user(&mut company.departments),
            Choice::REMOVE_USER => remove_user(&mut company.departments),
            Choice::QUERY_ALL_USERS => query_all_users(&company.departments),
            Choice::QUERY_DEPARTMENT => query_department(&company.departments),
            Choice::INVALID => invalid_message(),
            Choice::EXIT => break,
            _ => println!("unimplemented"),
        }
        let mut cont = String::new();
        io::stdin().read_line(&mut cont);
        clear_term();
    }
}

struct Company {
    name: String,
    departments: HashMap<String, Vec<Employee>>,
}

struct Department {
    name: String,
    employees: Vec<Employee>,
}

struct Employee {
    name: String,
    department: String,
}

enum Choice {
    ADD_USER,
    REMOVE_USER,
    QUERY_ALL_USERS,
    QUERY_DEPARTMENT,
    INVALID,
    EXIT,
}

fn menu_prompt() -> Choice {

    println!("Employee management system");
    println!("");
    println!("Enter the action you would like to take:");
    println!("1 - Add new user");
    println!("2 - Remove a user");
    println!("3 - Query department");
    println!("4 - Query company");
    println!("5 - Exit");

    print!("Enter choice: ");
    io::stdout().flush().unwrap();

    let mut user_input = String::new();
    io::stdin()
        .read_line(&mut user_input)
        .expect("Failed to read form stdin");

    user_input = user_input.trim().to_string();
    if user_input.len() != 1 {
        return Choice::INVALID;
    }

    let choice = user_input.chars().next().unwrap();
    match choice {
        '1' => Choice::ADD_USER,
        '2' => Choice::REMOVE_USER,
        '3' => Choice::QUERY_DEPARTMENT,
        '4' => Choice::QUERY_ALL_USERS,
        '5' => Choice::EXIT,
        _ => Choice::INVALID,
    }
}

/// Clear the terminal using ANSI Escape code
fn clear_term() {
    println!("{}[2J", 27 as char);
}

fn query_all_users(company_depts: &HashMap<String, Vec<Employee>>) {
    println!("");
    println!("");
    for (department, employees) in company_depts.iter() {
        println!("{}:", department);
        for employee in employees.iter() {
            println!("{}", employee.name);
        }
    }
    println!("");
    println!("");
}

fn query_department(company_depts: &HashMap<String, Vec<Employee>>) {
    println!("");
    println!("");
    let department = get_user_input("Department: ");
    let employees = company_depts.get(&department).unwrap();
    println!("{}:", department);
    for employee in employees.iter() {
        println!("{}", employee.name);
    }
    println!("");
    println!("");
}

fn remove_user(company_depts: &mut HashMap<String, Vec<Employee>>) {
    unimplemented!()
}

fn add_user(departments: &mut HashMap<String, Vec<Employee>>) {

    print!("Departments:");
    for key in departments.keys() {
        print!(" {}", key);
    }
    println!("");

    let name = get_user_input("Employee name: ");
    let department = get_user_input("Employee department: ");
    let employee = Employee {
        name,
        department,
    };

    let dept = departments.get_mut(&employee.department).unwrap();
    dept.push(employee);
}

fn invalid_message() {
    println!("Please only enter a valid choice from the list")
}

fn get_user_input(message: &str) -> String {
    print!("{}", message);
    io::stdout().flush().unwrap();

    let mut user_input = String::new();
    io::stdin()
        .read_line(&mut user_input)
        .expect("Failed to read form stdin");

    user_input.trim().to_string()
}
